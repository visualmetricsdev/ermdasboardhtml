jQuery(document).ready(function($) {
   //Animation Batterie Horizontal
   var batteriecharged = $('.battery-cons-charged').attr('data-charge');
   if (batteriecharged <= 10) {
     $('.c10').animate({opacity: '1'});
   } 
   if ((batteriecharged > 10) && (batteriecharged <= 25)) {
     $('.c10').animate({opacity: '1'});
     $('.c25').delay(500).animate({opacity: '1'});
   } 
    if ((batteriecharged > 25) && (batteriecharged <= 50)) {
     $('.c10').animate({opacity: '1'});
     $('.c25').delay(500).animate({opacity: '1'});
     $('.c50').delay(1000).animate({opacity: '1'});
   } 
  if ((batteriecharged > 50) && (batteriecharged <= 75)) {
     $('.c10').animate({opacity: '1'});
     $('.c25').delay(500).animate({opacity: '1'});
     $('.c50').delay(1000).animate({opacity: '1'});
     $('.c75').delay(1500).animate({opacity: '1'});
   } 
  if ((batteriecharged > 75) && (batteriecharged <= 90)) {
     $('.c10').animate({opacity: '1'});
     $('.c25').delay(500).animate({opacity: '1'});
     $('.c50').delay(1000).animate({opacity: '1'});
     $('.c75').delay(1500).animate({opacity: '1'});
     $('.c90').delay(2000).animate({opacity: '1'});
   } 
   if ((batteriecharged > 90) && (batteriecharged <= 100)) {
     $('.c10').animate({opacity: '1'});
     $('.c25').delay(500).animate({opacity: '1'});
     $('.c50').delay(1000).animate({opacity: '1'});
     $('.c75').delay(1500).animate({opacity: '1'});
     $('.c90').delay(2000).animate({opacity: '1'});
     $('.c100').delay(2500).animate({opacity: '1'});
   } 
  
  
   //Animation Batterie Vertical
   var batteriechargedv = $('.battery-cons-charged-vert').attr('data-charge');
   if (batteriechargedv <= 10) {
     $('.cv10').animate({opacity: '1'});
   } 
   if ((batteriechargedv > 10) && (batteriechargedv <= 25)) {
     $('.cv10').animate({opacity: '1'});
     $('.cv25').delay(500).animate({opacity: '1'});
   } 
    if ((batteriechargedv > 25) && (batteriechargedv <= 50)) {
     $('.cv10').animate({opacity: '1'});
     $('.cv25').delay(500).animate({opacity: '1'});
     $('.cv50').delay(1000).animate({opacity: '1'});
   } 
  if ((batteriechargedv > 50) && (batteriechargedv <= 75)) {
     $('.cv10').animate({opacity: '1'});
     $('.cv25').delay(500).animate({opacity: '1'});
     $('.cv50').delay(1000).animate({opacity: '1'});
     $('.cv75').delay(1500).animate({opacity: '1'});
   } 
  if ((batteriechargedv > 75) && (batteriechargedv <= 90)) {
     $('.cv10').animate({opacity: '1'});
     $('.cv25').delay(500).animate({opacity: '1'});
     $('.cv50').delay(1000).animate({opacity: '1'});
     $('.cv75').delay(1500).animate({opacity: '1'});
     $('.cv90').delay(2000).animate({opacity: '1'});
   } 
   if ((batteriechargedv > 90) && (batteriechargedv <= 100)) {
     $('.cv10').animate({opacity: '1'});
     $('.cv25').delay(500).animate({opacity: '1'});
     $('.cv50').delay(1000).animate({opacity: '1'});
     $('.cv5').delay(1500).animate({opacity: '1'});
     $('.cv90').delay(2000).animate({opacity: '1'});
     $('.cv100').delay(2500).animate({opacity: '1'});
   } 

   //Loading Weather
   $('.fadein-element').hide().fadeIn(1000);

});  


$(window).resize(function(){
   
    
});  




$( window ).load( function(){
    //Electricity Value Cost
    animateValue("value-cost", 0, 12, 2000);
    //Above Target
    animateValue("above-target", 0, 5, 2000);
     //Total Consumption
    animateValue("total-consumption", 0, 25, 2000);
     //Month Compare
    animateValue("month-compare", 0, -5, 2000);
});
/* VALUE ANIMATION */
function animateValue(id, start, end, duration) {
    var range = end - start;
    var current = start;
    var increment = end > start? 1 : -1;
    var stepTime = Math.abs(Math.floor(duration / range));
    var obj = document.getElementById(id);
    var timer = setInterval(function() {
        current += increment;
        obj.innerHTML = current;
        if (current == end) {
            clearInterval(timer);
        }
    }, stepTime);
}

/* CLOCK ANIMATION*/
jQuery('.pie').each(function(index, element) {
  var num = +($(this).text());
  var chart = '<svg viewBox="0 0 32 32"><circle class="circle" r="16" cx="16" cy="16" style="stroke-dasharray: 10 110" /></svg>';
  jQuery(this).html(chart);
  jQuery(this).find('.circle').css('stroke-dasharray', num + ' 100');
});


/* ANIMATION SCROLL */

// var $animation_elements = $('.animation-element');
// var $window = $(window);

// function check_if_in_view() {
//   var window_height = $window.height();
//   var window_top_position = $window.scrollTop();
//   var window_bottom_position = (window_top_position + window_height);

//   $.each($animation_elements, function() {
//     var $element = $(this);
//     var element_height = $element.outerHeight();
//     var element_top_position = $element.offset().top;
//     var element_bottom_position = (element_top_position + element_height);

//     //check to see if this current container is within viewport
//     if ((element_bottom_position >= window_top_position) &&
//         (element_top_position <= window_bottom_position)) {
//       $element.addClass('in-view');
//     } else {
//       //$element.removeClass('in-view');
//     }
//   });
// }
// $window.on('scroll', check_if_in_view);

/* CLOCK */
function clock() {// We create a new Date object and assign it to a variable called "time".
var time = new Date(),
    
    // Access the "getHours" method on the Date object with the dot accessor.
    hours = time.getHours(),
    
    // Access the "getMinutes" method with the dot accessor.
    minutes = time.getMinutes(),
    
    
    seconds = time.getSeconds();

document.querySelectorAll('.clock')[0].innerHTML = harold(hours) + ":" + harold(minutes);
  
  function harold(standIn) {
    if (standIn < 10) {
      standIn = '0' + standIn
    }
    return standIn;
  }
}
setInterval(clock, 1000);
/* TODAY DATE */
var today = new Date(),
    daylist = ["Sunday","Monday","Tuesday","Wednesday ","Thursday","Friday","Saturday"];
    monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
    dd = today.getDate(),
    mm = today.getMonth(),
    yyyy = today.getFullYear(),
    day = today.getDay(),
    day = daylist[day],
     mm = monthNames[mm];   
if (dd < 10 ) {
  dd = "0" + dd;
}
if (mm < 10 ) {
  mm = "0" + mm;
}
currentTime = day + ", " + dd + " " + mm;
$('.todaysdate').html(currentTime);





var doc = document.documentElement;
doc.setAttribute('data-useragent', navigator.userAgent);
//JQUery NoConflict
var $ = jQuery.noConflict();
// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
        window.getComputedStyle = function(el, pseudo) {
          this.el = el;
          this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
              prop = prop.replace(re, function () {
                return arguments[2].toUpperCase();
              });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
          }
          return this;
        }
 }


     